<?php
// Start the session immediatly
session_start();

// Let's throw our using statements here!
use DB3\Filter;
use HTMLForm\Input5;
use HTMLForm\Select;

// Autoload any classes we don't have
spl_autoload_register(function ($class) {
    // Define some paths that contain our classes
    $pathClasses = 'classes\\' .$class . '.php';
    $pathClassesA4 = 'classesa4\\' . $class . '.php';

    // If the original class path contains the class file
    if ( file_exists($pathClasses) )
    {
        // Load the class from the original class path
        require_once($pathClasses);
    }
    // If the assignment 4 class path contains the class file
    else if ( file_exists($pathClassesA4) )
    {
        // Load the class from the assignment 4 class path
        require_once($pathClassesA4);
    }
    //require_once '..\\classes\\' .$class . '.php';
    //require_once 'classesa4\\' .$class . '.php';
    // ..\classes\DB3\DB3.php
});

// Let's set up some variables for our form:
// This determines whether or not the user has been authenticated for this site
$isRegistered = false;
// Determines if the form has been posted yet
$isPosted = $_SERVER['REQUEST_METHOD'] === 'POST';

// Generate a Member object and fill it with the form input if the user
// has posted the form
$member = new Member();
if ($isPosted)
{
    $member->email = $_POST['email'];
    $member->password = $_POST['password'];
    $member->type = $_POST['type'];
}

// Check if the form the user posted is valid. If it is then
// check the database to see if the specified member exists
$isValidPost = $isPosted && $member->validate();
if ( $isValidPost )
{
    // Create the database so that we can find the memeber
    $db = new DB3A4('dba4/assign4.db');
    $db->exec($member->tableDefinition()); // Create the table as needed
    // Check if the member exists in the database
    $logins = $db->selectSome($member, [new Filter('email', $member->email)]);

    // Hash the given password
    $member->hashPassword();

    // Check if there are no pre-existing members extracted from the database.
    // Then see if we can insert our new member to the database
    $isRegistered = count($logins) === 0 && $db->insert($member);

    // Close and null the database to prevent memory leaks
    $db->close();
    $db = null;
}

// If our user is not authenticated, build the form that will be used on the page
if ( !$isRegistered )
{
    // Make the email field
    $emailInput = new Input5('email', 'email', $isPosted && !$isRegistered && $isValidPost ? '' : $member->email);
    $emailInput->addLabel($member->getLabel('email'));
    $emailInput->addError($isPosted && !$member->validate_email(), $member->getError('email'));

    // Make the password field
    $passwordInput = new Input5('password', 'password', $isPosted && !$isRegistered && $isValidPost ? '' : $member->password);
    $passwordInput->addLabel($member->getLabel('password'));
    $passwordInput->addError($isPosted && !$member->validate_password(), $member->getError('password'));

    // Make the type field
    $typeInput = new Select('type', [
        Member::STANDARD => 'Standard',
        Member::PREMIUM => 'Premium'], $member->type, '',
        'Please select a '.$member->getLabel('type') . '.', '');
    $typeInput->addLabel($member->getLabel('type'));
    $typeInput->addError($isPosted && !$member->validate_type(), $member->getError('type'));
}
// if the user is authenticated, let's setup a new session and redirect
else
{
    // Redirect to the login page
    header('Location: index.php');
    // stop execution of the rest of the code on this page
    die('Member is not registered');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Could Share Register</title>
    <link type="text/css" href="assign4.css" rel="stylesheet" />
</head>
<body>
    <!-- Navbar -->
    <?php require_once('generic/navbar.php') ?>
    <div class="main">
        <h1>Welcome to Cloud Share!</h1>
        <h2>Please enter your information so that we can register you into our system:</h2>

        <form action="#" method="post">
            <div>
                <?php $emailInput->render(); ?>
            </div>
            <div>
                <?php $passwordInput->render(); ?>
            </div>
            <div>
                <?php $typeInput->render(); ?>
            </div>
            <div>
                <input type="submit" value="Register!" />
            </div>
        </form>

        <?php if ( $isPosted && !$isRegistered && $isValidPost ) { ?>
        <div class="error">
            Email, <?= htmlentities($member->email) ?> is already registered.
        </div>
        <?php } ?>
    </div>
    
</body>
</html>