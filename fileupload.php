<?php
use HTMLTools\Table;
use HTMLForm\Input5;
#region Session/Cookie/Header Safe
// Start the session immediatly
require_once('generic/startmembersession.php');

// Let's set up some variables for our form:

// Determines if the form has been posted yet
$isPosted = $_SERVER['REQUEST_METHOD'] === 'POST' && count($_FILES) !== 0;
// Determines how many files can be uploaded at a time
$numOfFiles = $member->getFileInputConstraints();
// Contains a list of success/fail statuses for file uploads
$statuses = array();

// If the user has submitted the files they want to submit
if ($isPosted )
{
    // For every file the user wanted to submit
    foreach ( $_FILES as $sgFile )
    {
        // If the current file exists and was uploaded
        // to the "web server" in the temporary directory
        if ( is_array($sgFile) && !empty($sgFile['name']) )
        {
            // Create a new File object, add it to the postedFiles variable
            $file = File::createFileFromSuperglobal($sgFile, $member);

            if ( $file->validate() )
            {
                // Move the uploaded file to the uploadsa4 folder
                if ( move_uploaded_file($sgFile['tmp_name'], $file->path) )
                {
                    // Open the database
                    $db = new DB3A4('dba4/assign4.db');
                    // Create the File table if it doesn't exist
                    $db->exec($file->tableDefinition());
                    // Add the file to the database
                    $db->insert($file);
                    // Close the database
                    $db->close();
                    $db = null;
                    // add a success status if the file successfully moved
                    $statuses[] = new FileStatus($file->name, FileStatus::SUCCESS);
                }
                else
                {
                    //add a fail status if the file failed to move
                    $statuses[] = new FileStatus($file->name, FileStatus::FAIL,
                        'Couldn\'t move the file onto the server.');
                }
            }
            else
            {
                if ( !$file->validate_type() )
                {
                    // Show an error if the file type is invalid
                    $statuses[] = new FileStatus($file->name, FileStatus::FAIL,
                        $file->getError('type'));
                }
                else
                {
                    // Show an error if the file size is invalid
                    $statuses[] = new FileStatus($file->name, FileStatus::FAIL,
                        $file->getError('size'));
                }
            }
        }
    }

    // Build the table to be used to present the file upload summary
    $table = new Table($statuses);
}
else
{
    // Build the file input fields
    $fileInputs = array();
    for ($i = 0; $i < $numOfFiles; $i++)
    {
        $fileInputs[] = new Input5("file$i", 'file');
    }
}
#endregion
?>
<!DOCTYPE html>
<html>
<head>
    <title>Cloud Share Upload FIles</title>
    <link type="text/css" href="assign4.css" rel="stylesheet" />
</head>
<body>
    <!-- Navbar -->
    <?php require_once('generic/navbar.php') ?>
    <div class="main">
        <?php if (!$isPosted) { ?>
        <h1>Upload a file!</h1>
        <h2>
            Please choose <?= $numOfFiles ?> files to upload
        </h2>
        <form action="#" method="post" enctype="multipart/form-data">
            <?php foreach ( $fileInputs as $input ) { ?>
            <div>
                <?php $input->render(); ?>
            </div>
            <?php } ?>
            <input type="submit" value="Upload!" />
        </form>
        <?php } else { ?>
        <h1>File Upload Results</h1>
        <div>
            <?php $table->render(); ?>
        </div>
        <?php } ?>
    </div>
</body>
</html>
