<?php
#region Session/Cookie/Header Safe
// Start the session immediatly
require_once('generic/startmembersession.php');

// Set up page variables
$isPosted = $_SERVER['REQUEST_METHOD'] === 'POST'; // Whether or not the user posted the form

// Make sure that the current logged in user is the same user logging out
if ($isPosted && $member->id == $_POST['userId'])
{
    // Unset session variables
    unset($_SESSION['member']);
    session_unset();

    // Redirect user to login page
    header('Location: /index.php');
    die('You have logged off');
}
#endregion


?>
<!DOCTYPE html>
<html>
<head>
    <title>Cloud Share Logoff</title>
    <link type="text/css" href="assign4.css" rel="stylesheet" />
</head>
<body>
    <?php require_once 'generic/navbar.php'; ?>
    <div class="main">
        <h1>Cloud Share Logoff</h1>
        <h2>
            Are you sure you want to log off <?= htmlentities($member->alias) ?>?
        </h2>
        <form action="#" method="post">
            <!-- Hidden form input with the ID of the current user -->
            <input type="hidden" name="userId" value="<?= $member->id ?>" />
            <input type="submit" value="Logoff!" />
        </form>
    </div>

</body>
</html>
