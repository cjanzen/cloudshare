<?php
namespace HTMLTools;
spl_autoload_register(function ($class){
    require_once '..\\' . $class . '.php';
});
/**
 * Table class generates an html table from an array of objects
 * @version 1.0
 * @author Ernesto Basoalto
 */
class Table
{
    protected $html;
    private $fields;

    public function __construct($items,$header=true,$attributeString='')
    {
        $this->fields = array_keys(get_object_vars($items[0]));
        $head = !$header? '' : $this->getHeader($items[0]);
        foreach($items as $class)
        {
            $this->html.= $this->getRow($class);
        }
        $this->html = <<<EOT

    <table $attributeString>
$head
        <tbody>
{$this->html}
        </tbody>
    </table>
EOT;
    }

    public function render()
    {
        echo $this->html;
    }

    private function getHeader($class)
    {
        $row = '';
        foreach ($this->fields as $col)
        {
            $val = $class->getLabel($col);
            $row.= "\t\t\t<th>$val</th>\n";
        }
        return <<<EOT

        <thead>
        <tr>
$row
        </tr>
        </thead>
EOT;
    }

    private function getRow($class)
    {
        $td = '';
        foreach ($this->fields as $col)
        {
            $val = htmlentities($class->$col);
            $td.= <<<EOT

            <td>
                $val
            </td>
EOT;
        }
        return <<<EOT

        <tr>
$td
        </tr>
EOT;
    }
}