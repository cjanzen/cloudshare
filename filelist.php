<?php
#region Session/Cookie/Header Safe
// Start the session immediatly
require_once('generic/startmembersession.php');
#endregion

use DB3\Filter;

// Create a temporary file object to get labels
$tempFile = new File();

#region Deleting From Database
$isPosted = $_SERVER['REQUEST_METHOD'] === 'POST';
// If the admin specified files to delete
if ( $isPosted && isset($_POST['filesToDelete']) && $member->type === Member::ADMIN )
{
    // Make an array of Files that have the id of the file to delete
    $deleteFiles = array();
    foreach ( $_POST['filesToDelete'] as $id )
    {
        // Open the database
        $db = new DB3A4('dba4/assign4.db');
        // Grab the file that we need to delete
        $file = new File($id);
        $db->select($file);
        // Close the database
        $db->close();
        $db = null;
        // Delete the file from the filesystem if it exists
        if ( file_exists($file->path) )
        {
            unlink($file->path);
        }
        $deleteFiles[] = $file;
    }

    // If the delete files array isn't empty (there are files to delete)
    if ( count($deleteFiles) !== 0 )
    {
        // Open the database
        $db = new DB3A4('dba4/assign4.db');

        // Delete the files we need to delete
        $db->deleteSome($deleteFiles);

        // Close the database
        $db->close();
        $db = null;
    }
}
#endregion
?>
<!DOCTYPE html>
<html>
<head>
    <title>Cloud Share File List</title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js"></script>
    <script type="text/javascript">
        /* Functions */
        // A function we will use later to retrieve file JSON from the server
        // with AJAX
        function getJSON()
        {
            $.ajax({
                datatype: "json",
                url: "filejson.php",
                data: {'search': $('#txtSearch').val(), 'orderby': $('input:radio[name="orderby"]:checked').val()},
                success: viewModel.files,
                method: 'GET'
            });
        }
        //function postJSON()
        //{
        //    $.ajax({
        //        datatype: "json",
        //        url: "filejson.php",
        //        data: {'name': $('#txtName').val(), 'type': $('#txtType').val(), 'orderby': 'Fill this section later'},
        //        success: viewModel.files,
        //        method: 'POST'
        //    });
        //}

        /* Logic */
        // Create a viewModel object that will bind the file list to a table
        var viewModel = {
            files: ko.observableArray()
        };

        // Use the getJSON method set the viewModel to hold the list of files from filejson.php
        getJSON();

        // Determines if the Delete button was pressed
        var deleteIsPressed = false;

        // Bind the file list to the DOM
        $(function () {
            ko.applyBindings(viewModel);

            // Set event handler for the search box
            $("#txtSearch").keypress(function (e) {
                if (e.which === 13) {
                    // Search using the given criteria
                    getJSON();
                }
            })
        });
    </script>
    <link type="text/css" href="assign4.css" rel="stylesheet" />
</head>
<body>
    <!-- Navbar -->
    <?php require_once('generic/navbar.php') ?>
    <form onsubmit="return deleteIsPressed;" action="#" method="post" class="main">
        <h1>Cloud Share File List</h1>
        <div>
            <?php if ($member->type === Member::ADMIN) { ?>
            <input type="text" id="txtSearch" />
            <button onclick="getJSON();" type="button">Search!</button>
            <?php } ?>
        </div>
        <table>
        <thead>
                <tr class="header">
                    <th><label for="rdoName"><?= $tempFile->getLabel('name') ?></label><input id="rdoName" name="orderby" type="radio" value="name" onclick="getJSON();" /></th>
                    <th><label for="rdoType"><?= $tempFile->getLabel('type') ?></label><input id="rdoType" name="orderby" type="radio" value="type" onclick="getJSON();" /></th>
                    <th><label for="rdoSize"><?= $tempFile->getLabel('size') ?></label><input id="rdoSize" name="orderby" type="radio" value="size" onclick="getJSON();" /></th>
                    <?php if ($member->type === Member::ADMIN) { ?>
                    <th>Delete</th>
                    <?php } ?>
                </tr>
            </thead>
            <!-- Tell KO to bind the file list here -->
            <tbody data-bind="foreach: files">
                <tr>
                    <td><a data-bind="text: name, attr: {href: path}"></a></td>
                    <td data-bind="text: type"></td>
                    <td data-bind="text: size"></td>
                    <?php if ($member->type === Member::ADMIN) { ?>
                    <td><input type="checkbox" name="filesToDelete[]" data-bind="value: id" /></td>
                    <?php } ?>
                </tr>
            </tbody>
        </table>
        <?php if ($member->type === Member::ADMIN) { ?>
        <div>
            <input onmouseup="deleteIsPressed = true;" type="submit" value="Delete!" />
        </div>
        <?php } ?>
    </form>
    
</body>
</html>
