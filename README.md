# CloudShare
A simple file sharing web application I did for a PHP class. Allows users to upload files to the web server for other users to view/download.

Users can register into the CloudShare system with an email address, a password, and a membership type. Passwords are hashed prior to being stored in the database for security. Users can have two different kinds of membership (with an additional hidden type that has to be manually entered into the database):

 1. **Standard**
	 * Can only upload two files at a time
	 * Can only upload text and image files that are less than 100 KB in size
	 * Can only see text and image files on the File List page
2. **Premium**
	* Can upload five files at a time.
	* Can only upload text, image, and video files that are 300 KB in size
	* Can only see text, image, and video files on the File List page.
3. **Admin**
	* Can upload five files at a time.
	* Can upload any file of any size.
	* Can see all uploaded files on the File List page.
	* Can delete any file from the File List page.
	* Can search for files by name and type.
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

You will need the following in order to run this web application on your machine:

1. **Visual Studio**: The IDE used to develop this web application. Can be downloaded from https://www.visualstudio.com/.
2. **PHP Tools**: The Visual Studio extension needed for PHP development. Can be installed via Visual Studio's "Extensions and Updates"

### Installing

Make sure that you install Visual Studio and the PHP Tools extension. The extension can be installed by clicking on "Tools" in the Menu Bar and clicking "Extensions and Updates."

Once both are installed, you will need to modify the "php.ini" file (usually found in the "C:\Program Files (x86)\IIS Express\PHP\v7.1\" directory) like so:

```
[sqlite3]
sqlite3.extension_dir = "C:\Program Files (x86)\IIS Express\PHP\v7.1\ext\"

[...]

[ExtensionList]
extension=php_sqlite3.dll
```

Save the file. You can open the "CloudShare.sln" file in Visual Studio. Open the "index.php" file in Visual Studio and press Ctrl+F5 to open the login page.

### Usage

You can either login as the pre-made admin user (email: a@a.ca, password: pw@rd111) or make a new user with the "Register" link. The password needs to be a 8-16 character password with at least one number and one special character.

Once logged in, you can see a list of files with the "List" link, upload files with the "Upload" link, or log out using the "Logout" link.

**Note**: If the uploads don't work, try adding an "uploadsa4" folder in the root folder of the project (where the "CloudShare.sln" file is).

On the File List page, you can

 - Order files in ascending order by clicking on the table headers.
 - (Admin only) Search for a file by name and/or type.
 - (Admin only) Delete files by checking which files you want to delete.

## Built With

* [Visual Studio](https://www.visualstudio.com/) - IDE used.
* [PHP Tools](https://www.devsense.com/) - PHP extension for Visual Studio.

## Authors

* **Corey Janzen**  - [cjanzen](https://bitbucket.org/cjanzen/)



> Written with [StackEdit](https://stackedit.io/).