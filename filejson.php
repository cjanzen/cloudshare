<?php
#region Session/Cookie/Header Safe
// Start the session immediatly
require_once('generic/startmembersession.php');
// Include use statements here!
use DB3\Filter;

header('Content-type:application/json');
#endregion

// Grab some filters and an orderby from the GET superglobal
$filters = array();
$orderby = isset($_GET['orderby']) ? $_GET['orderby'] : '';

if ( $member->type === Member::ADMIN )
{
    // If the search value isn't blank
    if (!empty($_GET['search']))
    {
        // Add this field to the filter list
        $filters[] = new Filter('name', '%'.$_GET['search'].'%', 'LIKE');
        $filters[] = new Filter('type', '%'.$_GET['search'].'%', 'LIKE');
    }
}
else
{
    // Add some filters for the premium/standard member so they only
    // see text, image files
    $filters[] = new Filter('type', 'text/%', 'LIKE');
    $filters[] = new Filter('type', 'image/%', 'LIKE');
    if ($member->type === Member::PREMIUM)
    {
        // include video files for premium members
        $filters[] = new Filter('type', 'video/%', 'LIKE');
    }
}


// Open the database
$db = new DB3A4('dba4/assign4.db');

// Grab some files using the filters we got earlier.
// Sort them by the specified field
$files = $db->selectSomeOrder(new File(), $filters, false, $orderby, DB3A4::ASC);

// Close the database
$db->close();
$db = null;

// Echo out some JSON using the files we just retrieved from the database
echo json_encode($files);