<?php
// Start the session immediatly
session_start();

// Autoload any classes we don't have
spl_autoload_register(function ($class) {
    // Define some paths that contain our classes
    $pathClasses = 'classes\\' .$class . '.php';
    $pathClassesA4 = 'classesa4\\' . $class . '.php';

    // If the original class path contains the class file
    if ( file_exists($pathClasses) )
    {
        // Load the class from the original class path
        require_once($pathClasses);
    }
    // If the assignment 4 class path contains the class file
    else if ( file_exists($pathClassesA4) )
    {
        // Load the class from the assignment 4 class path
        require_once($pathClassesA4);
    }
    //require_once '..\\classes\\' .$class . '.php';
    //require_once 'classesa4\\' .$class . '.php';
    // ..\classes\DB3\DB3.php
});

// Make a member object if the user is logged in
$member = isset($_SESSION['member']) ?
    Member::createMemberFromSession($_SESSION['member']) : null;

// Check if the user is logged on
if ( !isset($_SESSION['member']) )
{
    // If the user isn't logged on, redirect them to login page
    header('Location: index.php');
    // stop execution of the rest of the code on this page
    die('Member is not authenticated');
}