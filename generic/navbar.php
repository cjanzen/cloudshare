<ul id="navbar" role="navigation">
    <?php if (isset($member->id)) { ?>
    <li><span class="user">Welcome <?= htmlentities($member->email) ?></span></li>
    <li><a href="filelist.php">List</a></li>
    <li><a href="fileupload.php">Upload</a></li>
    <li><a href="logout.php">Logout</a></li>
    <?php } else { ?>
    <li><span class="user">Welcome</span></li>
    <li><a href="index.php">Login</a></li>
    <li><a href="register.php">Register</a></li>
    <?php } ?>
</ul>