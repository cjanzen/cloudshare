<?php
use DB3\Model;
use DB3\Type;

/**
 * This class represents members who are a part of the Cloud Share system.
 *
 * Each member has 3 publicly-avaiable attributes:
 * An id for the member,
 * an email address for the member, and
 * an encrypted password.
 *
 * @version 1.0
 * @author cst219
 */
class Member extends Model
{
    #region Class Constants
    /* CLASS CONSTANTS */
    /**
     * An integer that represents the standard membership type
     */
    const STANDARD = 'standard';
    /**
     * An integer that represents the premium membership type
     */
    const PREMIUM = 'premium';
    /**
     * An integer that represents the admin membership type
     */
    const ADMIN = 'admin';

    /**
     * The maximum file size a standard member can upload
     * in bytes. (100 KB)
     */
    const STANDARD_SIZE = 102400;
    /**
     * The maximum file size a standard member can upload
     * in bytes. (300 KB)
     */
    const PREMUIM_SIZE = 307200;
    #endregion

    #region Class Attributes
    /* CLASS ATTRIBUTES */
    /**
     * Determines how many characters can be in the password.
     * Default is 16 characters.
     * NOTE: change this length when we hash the password so that we can pass validation
     * when we insert.
     * @var int
     */
    private $maxPasswordLength;

    /**
     * The id for our member
     * @var int
     */
    public $id;


    /**
     * The email address for our member
     * @var string
     */
    public $email;
    /**
     * Validates the email provided by the user in this Member object.
     * Should be valid when the email attribute is filled with an
     * email address-formatted string.
     *
     * The errors can be found with the getError method.
     */
    public function validate_email()
    {
        return $this->checkProperty('email', !empty($this->email), '%s is required.') &&
            $this->checkProperty('email', filter_var($this->email, FILTER_VALIDATE_EMAIL),
            '%s should be a valid email address');
    }


    /**
     * The encrypted password for our member
     * @var string
     */
    public $password;
    /**
     * Validates the password provided by the user in this Member object.
     * Should be valid when the password is filled and is 8 to 16 characters
     * long.
     *
     * The errors can be found with the getError method.
     */
    public function validate_password()
    {
        $length = strlen($this->password); // The length of the password
        return $this->checkProperty('password', !empty($this->password), '%s is required.') &&
            $this->checkProperty('password', $length >= 8 && $length <= $this->maxPasswordLength,
            '%s must be between 8 to 16 characters long');
    }

    /**
     * The membership type for our member
     * @var string
     */
    public $type;
    /**
     * Validates the membership type provided by the user in this Member object.
     * Should be valid when they are a standard member or a premium member
     *
     * The errors can be found with the getError method.
     */
    public function validate_type()
    {
        return $this->checkProperty('type', isset($this->type) && !empty($this->type),
            '%s is required. Please select one from the list.') &&

            $this->checkProperty('type', $this->type === self::STANDARD ||
                                         $this->type === self::PREMIUM,
                                         '%s is an invalid type. Please select one from the list.');
    }

    #endregion

    #region Class Constructor
    /* CLASS CONSTRUCTOR */
    /**
     * Constructs a Member object to be used to represent a member of the Cloud Share application
     * @param int $id - the id of the member
     * @param string $email - The email address of the member.
     * @param string $password - The member's encrypted password.
     * @param string $type - The membership type of this member
     */
    public function __construct($id=null, $email=null, $password=null, $type=null)
    {
        // Set up the class attributes
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->type = $type;
        $this->maxPasswordLength = 16;

        // Define some columns for a database table
        $this->defineColumn('id', Type::INT, null, false, true, true);
        $this->defineColumn('email', Type::TXT, null, false);
        $this->defineColumn('password', Type::TXT, 255, false);
        $this->defineColumn('type', Type::TXT, null, false);

        // Add some labels for each attribute (except id)
        $this->setLabel('email', 'Email Address');
        $this->setLabel('password', 'Password');
        $this->setLabel('type', 'Membership Type');
    }
    #endregion

    #region Class Methods
    /**
     * Hashes the current password in this Member object.
     * Also ensures that validation will still pass, even with a hashed
     * password.
     */
    public function hashPassword()
    {
        // Set this classes password to a hashed version of the password
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        // Change the max password length so that validation will still
        // pass with the hashed password
        $this->maxPasswordLength = 255;
    }

    /**
     * Determines the number of files that this user can upload based
     * on their membership type. Standard members get 2 file uploads
     * while premium and admin members get 5.
     * @return integer
     */
    public function getFileInputConstraints()
    {
        // The return value that will determine the number of file
        // uploads they can do. Standard members gets 2 file uploads
        $numOfFiles = 2;

        // If the member is a premium or admin member
        if ( $this->type === self::PREMIUM || $this->type === self::ADMIN )
        {
            // Change the return value to 5 so that they can upload 5 files
            $numOfFiles = 5;
        }

        // return the number of files they can upload
        return $numOfFiles;
    }

    /**
     * Returns an integer representing the maximum size of a file
     * that a member can upload.
     * @return integer
     */
    public function getFileSizeConstraints()
    {
        // The return value for this function
        $returnSize = 0;

        switch ($this->type)
        {
            // if the member is a standard member, return 100 KB as
            // the maximum size
            case self::STANDARD:
                $returnSize = self::STANDARD_SIZE;
                break;
            // if the member is a premium member, return 300 KB as
            // the maximum size
            case self::PREMIUM:
                $returnSize = self::PREMUIM_SIZE;
                break;
        }

        // Return the max size
        return $returnSize;
    }

    /**
     * Simply provides an error message for an invalid file
     * size as a string.
     * @return string
     */
    public function getInvalidSizeError()
    {
        return 'The file size was larger than ' . $this->getFileSizeConstraints()/1024
            . ' KB';
    }

    /**
     * Creates a Member object from a Session variable (assuming
     * it was holding a Member object at the time it was being
     * saved to the session variable)
     * @param mixed $sMember - the Member object from the session
     *                         variable
     * @return Member
     */
    public static function createMemberFromSession($sMember)
    {
        // Grab the object variables from the given session variable
        $sMemberVars = get_object_vars($sMember);

        // Create a new Member object based on the given session
        // variable and return it.
        return new Member( $sMemberVars['id'], $sMemberVars['email'],
            $sMemberVars['password'], $sMemberVars['type'] );
    }
    #endregion
}