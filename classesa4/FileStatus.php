<?php
use DB3\Model;

/**
 * The FileStatus class is used to instantiate objects that represent the
 * success/fail status of file uploads. Also provides the reason why the
 * file failed to upload.
 *
 * @version 1.0
 * @author cst219
 */
class FileStatus extends Model
{
    #region Class Constants
    /**
     * A string constant that holds a success status string
     */
    const SUCCESS = "Success";
    /**
     * A string constant that holds a fail status string
     */
    const FAIL = "Fail";
    #endregion

    #region Class Atrributes
    /**
     * The name of the file that was uploaded.
     * @var string
     */
    public $filename;
    /**
     * The success/failed status of the file upload
     * @var string
     */
    public $status;
    /**
     * The reason why the upload failed, if it did.
     * @var string
     */
    public $reason;
    #endregion

    #region Class Constructor
    /**
     * Constructs a FileStatus object used to report to the user
     * on the status of a file upload
     * @param string $filename - the name of the file that was being uploaded
     * @param string $status - the success/failure of the file upload
     * @param string $reason - (OPTIONAL) the reason the file failed to upload.
     *                         Default is a blank string.
     */
    public function __construct($filename, $status, $reason="")
    {
        // Set up the class attributes
        $this->filename = $filename;
        $this->status = $status;
        $this->reason = $reason;

        // Set up labels
        $this->setLabel('filename', 'Filename');
        $this->setLabel('status', 'Status');
        $this->setLabel('reason', 'Reason');
    }
    #endregion
}