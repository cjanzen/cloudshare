<?php
use DB3\Model;
use DB3\Type;

/**
 * This class represents files that have been uploaded to the Cloud Share system.
 *
 * Each File has 5 publicly-available attributes:
 * The id of the file,
 * the name of the file,
 * the relative path to the file in uploadsa4,
 * the type of the file, and
 * the size of the file (in bytes).
 *
 * @version 1.0
 * @author cst219
 */
class File extends Model
{
    #region Class Attributes
    /**
     * The member that "owns" this file
     * @var Member
     */
    private $member;

    /**
     * The id of this file as an auto-incrementing integer
     * @var int
     */
    public $id;

    /**
     * The name of the file as a string
     * @var string
     */
    public $name;
    /**
     * Validates the file name provided by the user in this File object.
     * Should be valid when the name isn't blank.
     *
     * The errors can be found with the getError method.
     * @return boolean
     */
    public function validate_name()
    {
        return $this->checkProperty('name', !empty($this->name), '%s is required.');
    }

    /**
     * The relative path to the file in uploadsa4.
     * @var string
     */
    public $path;
    /**
     * Validates the file path provided by the fileupload.php page when the member
     * uploads this File object to the database.
     * Should be valid when the path isn't blank.
     *
     * The errors can be found with the getError method.
     * @return boolean
     */
    public function validate_path()
    {
        return $this->checkProperty('path', !empty($this->path), '%s is required.');
    }

    /**
     * The mime type of this file as a string
     * @var string
     */
    public $type;
    /**
     * Validates the file type indirectly provided by the user in this File object.
     * Should be valid when the type isn't blank and is a properly-formatted mime type.
     *
     * The errors can be found with the getError method.
     * @return boolean
     */
    public function validate_type()
    {
        return $this->checkProperty('type', !empty($this->type), '%s is required.') &&
            $this->checkProperty('type', strpos('/', $this->type) !== -1,
                    '%s needs to be a properly-formatted mime type') &&
                $this->checkProperty('type', $this->isFileTypeValid($this->type),
                    '%s is invalid: '. 'You can\'t upload this type of file.'
                                     . ' Consider upgrading your Membership Type.');
    }

    /**
     * The size of this file (in bytes) as an integer
     * @var int
     */
    public $size;
    /**
     * Validates the file size indirectly provided by the user in this File object.
     * Should be valid when the size isn't blank and is an integer.
     *
     * The errors can be found with the getError method.
     * @return boolean
     */
    public function validate_size()
    {
        return $this->checkProperty('size', !empty($this->size), '%s is required') &&
            $this->checkProperty('size', is_int($this->size), '%s isn\'t a number.') &&
            $this->checkProperty('size', $this->isFileSizeValid($this->size),
                '%s is invalid: '.$this->member->getInvalidSizeError());
    }
    #endregion

    #region Class Constructor
    /**
     * Constructs a file object that represents a file in the database
     * @param Member $member - The member that "owns" this file
     * @param int $id - The id of the file
     * @param string $name - the original filename of this file
     * @param string $path - the relative path to the file in the uploadsa4 directory
     * @param string $type - the mime type of the file
     * @param int $size - the size of the file in bytes.
     */
    public function __construct(int $id=null, string $name=null, string $path=null, string $type=null, int $size=null, $member=null)
    {
        // Set up the class attributes
        $this->member = $member;
        $this->id = $id;
        $this->name = $name;
        $this->path = $path;
        $this->type = $type;
        $this->size = $size;

        // Define some columns for the table
        $this->defineColumn('id', Type::INT, null, false, true, true);
        $this->defineColumn('name', Type::TXT, null, false);
        $this->defineColumn('path', Type::TXT, null, false);
        $this->defineColumn('type', Type::TXT, null, false);
        $this->defineColumn('size', Type::INT, null, false);

        // Set up some labels
        $this->setLabel('name', 'Name');
        $this->setLabel('type', 'File Type');
        $this->setLabel('size', 'Size');
    }


    /**
     * Creates a new File object based on one of the files from the
     * $_FILES superglobal.
     * @param mixed $sgFile - One of the files from the $_FILES super global (ex. $_FILES['file1'])
     * @param Member $member - The member that owns this file
     * @return File
     */
    public static function createFileFromSuperglobal($sgFile, $member)
    {
        // Create a new File object and fill in the attributes
        // based on the passed-in superglobal file. Then return
        // this object.
        return new File(null, $sgFile['name'], 'uploadsa4/'.uniqid().$sgFile['name'], $sgFile['type'], $sgFile['size'], $member);
    }

    /**
     * Determines if the member can upload the file of the specified type
     * depending on the membership type.
     * Standard members can only upload text and image files,
     * Premuim members can only upload text, image, and video files, and
     * Admin members can upload any type of file.
     * @param string $type - the type of file the member is trying to upload.
     * @return boolean
     */
    public function isFileTypeValid(string $type)
    {
        // A boolean return value that will determine if the member
        // can upload this file or not
        $isValidFileType = false;

        // if the member is a standard member
        if ($this->member->type === Member::STANDARD)
        {
            // Only return true if this is a text or image file
            $isValidFileType = strpos($type, 'text') === 0 ||
                               strpos($type, 'image') === 0;
        }
        // If the member is a premium member
        else if ( $this->member->type === Member::PREMIUM )
        {
            // Only return true if this is a text, image, or video file
            $isValidFileType = strpos($type, 'text') === 0 ||
                               strpos($type, 'image') === 0 ||
                               strpos($type, 'video') === 0;
        }
        // If this is an admin member
        else if ( $this->member->type === Member::ADMIN )
        {
            // return true regardless of file type
            $isValidFileType = true;
        }
        // A memeber with an invalid type shouldn't be able to
        // upload files regardless of file type.

        // Return the return value
        return $isValidFileType;
    }

    /**
     * Determines if the member can upload the file of the specified size
     * depending on the membership type.
     * Standard members can only upload files of less than 100 KB,
     * Premium members can only upload files of less than 300 KB, and
     * Admin members can upload files of any size.
     * @param int $size - the size of the file the user is trying to upload.
     * @return boolean
     */
    public function isFileSizeValid(int $size)
    {
        // A boolean return value that will determine if the member
        // can upload this file or not
        $isValidFileSize = false;

        // if the member is a standard or premium member
        if ($this->member->type === Member::STANDARD || $this->member->type === Member::PREMIUM)
        {
            // Only return true if the file size is less than their max size
            $isValidFileSize = $size < $this->member->getFileSizeConstraints();
        }
        // If this is an admin member
        else if ( $this->member->type === Member::ADMIN )
        {
            // Return true regardless of file size
            $isValidFileSize = true;
        }
        // A memeber with an invalid type shouldn't be able to
        // upload files regardless of file size.

        // Return the return value
        return $isValidFileSize;
    }
    #endregion
}