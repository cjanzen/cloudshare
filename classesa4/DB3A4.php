<?php
use DB3\DB3;

/**
 * An extended version of the DB3 class that adds the ability to select some rows
 * from the database in a certain order.
 *
 * @version 1.0
 * @author cst219
 */
class DB3A4 extends DB3
{
    #region Class Constants
    /* CLASS CONSTANTS */

    /**
     * Constant for selectSomeOrder that tells that method to order
     * items going from least to greatest.
     */
    const ASC = true;
    /**
     * Constant for selectSomeOrder that tells that method to order
     * items going from greatest to least.
     */
    const DESC = false;
    #endregion

    #region Class Methods
    /* CLASS METHODS */
    /**
     * Returns an array of objects that meet the criteria specified by the array of Filter objects
     * and orders them by the specified field in the specified order
     * @param \DB3\Model $class - the object whose table we are querying. Assumes that this object extends the Model class
     * @param mixed $filters - array of filter objects used to create a where clause. Default is no filters.
     * @param bool $andOperator - whether to use the AND operator or the OR operator to connect the filter expressions
     *                            Default is the AND operator
     * @param mixed $orderField - the name of the field you want to order by. Default is nothing.
     * @param bool $order - the Ascending/Descending order to present the items in. You can used the
     *                      ASC and DESC constants to determine the order. Default is Ascending order.
     */
    public function selectSomeOrder($class, $filters=array(), $andOperator=true, $orderField='', $order=true)
    {
        // Grab the table name
        $table = $class->tableName();
        // Grab the table attributes
        $fieldValues = get_object_vars($class);
        // Make a comma-separated list of table attributes
        $fieldList = implode(',', array_keys($fieldValues));
        // Check if the order by field is valid
        $isOrderValid = array_key_exists($orderField, $fieldValues);

        // Let's see if all the filters are valid
        $i=0;
        foreach($filters as $filt)
        {
            // If the filter's field doesn't exists in the specified class
            if ( !array_key_exists($filt->field, $fieldValues) )
            {
                // Remove the filter from the array
                unset($filters[$i]);
            }

            // Check the next index
            $i++;
        }

        // if the order by field is invalid
        if ( !$isOrderValid )
        {
            // Use the selectSome method instead to get the fields and return that
            return $this->selectSome($class, $filters, $andOperator);
        }
        else
        {
            // Continue on preparing a statement with a ORDER BY

            // Determine which operator to connect the filters
            // either use the AND operator or the OR operator
            $oper = $andOperator ? ' AND ' : ' OR ';

            // Set up the order by statement
            $orderBy = "ORDER BY $orderField " . ($order ? 'ASC' : 'DESC');

            // convert the filters to strings and implode them with the operator determined aboce
            $filterList = empty($filters) ? '1=1' : implode($oper, $filters); // if no filters use a where clause that returns all rows

            // Prepare a statement with the field list, the table, the filter list, and the order by statement
            $stmt = $this->prepare("SELECT $fieldList FROM $table WHERE $filterList $orderBy");

            // Bind the filters to the statement
            $i=1;
            foreach($filters as $filt){
                $stmt->bindValue($i, $filt->value,
                    is_null($filt->value) ? SQLITE3_NULL : $class->getBindType($filt->field)
                );//assume that class extends Model and has the getBindType method
                $i++;
            }

            // Execute the statement
            $result = $stmt->execute();

            if(!$result)
            {
                // Return false if the statement failed to produce any result rows
                $stmt->close();
                return false;
            }
            else
            {
                $items = array(); //create blank array to fill and return
                while($row = $result->fetchArray(SQLITE3_ASSOC))
                {
                    $item = new $table(); //using class name to create an instance of this class
                    foreach($row as $col=>$val)
                    {
                        $item->$col = $val; //set the calue of the class property with the same name as the column
                    }

                    $items[]=$item; //add the new class to the return array
                }
                // Close statement, return the items array
                $stmt->close();
                return $items;
            }
        }
    }


    /**
     * Summary of deleteSome
     * @param \DB3\Model[] $classes
     */
    public function deleteSome($classes)
    {
        // Only run this function if the classes paramter is an
        // array and isn't empty
        if ( is_array($classes) && count($classes) !== 0 )
        {
            // Grab the table name
            $table = $classes[0]->tableName();
            // Get the primary key field
            $pk = $classes[0]->getPKField();
            // Get the SQLITE3 data type of the pk
            $pkType = $classes[0]->getBindType($pk);
            // Get a list of ids and Qmarks for our delete statement
            $idArray = array();
            $qmarks = '';
            foreach ( $classes as $class )
            {
                $idArray[] = $class->$pk;
                $qmarks .= '?,';
            }
            $qmarks = rtrim($qmarks, ',');

            // Create a prepared statement that will delete the specified
            // classes from the table
            $stmt = $this->prepare("DELETE FROM $table WHERE $pk IN ($qmarks)");
            $i = 1;
            foreach ( $idArray as $id )
            {
                $stmt->bindValue($i, $id, $pkType);
                $i++;
            }

            // Run the Statement
            $result = $stmt->execute();

            if(!$result)
            {
                // Return false if the statement failed to produce any result rows
                $stmt->close();
                return false;
            }
            else
            {
                // Close statement, return the items array
                $stmt->close();
                return true;
            }
        }
        else
        {
            // Return false if the user didn't provide the right parameter
            return false;
        }

    }
    #endregion
}