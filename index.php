<?php
#region Session/Cookie/Header Safe
// Start the session immediatly
session_start();

// Let's throw our using statements here!
use DB3\Filter;
use HTMLForm\Input5;



// Autoload any classes we don't have
spl_autoload_register(function ($class) {
    // Define some paths that contain our classes
    $pathClasses = 'classes\\' .$class . '.php';
    $pathClassesA4 = 'classesa4\\' . $class . '.php';

    // If the original class path contains the class file
    if ( file_exists($pathClasses) )
    {
        // Load the class from the original class path
        require_once($pathClasses);
    }
    // If the assignment 4 class path contains the class file
    else if ( file_exists($pathClassesA4) )
    {
        // Load the class from the assignment 4 class path
        require_once($pathClassesA4);
    }
    //require_once '..\\classes\\' .$class . '.php';
    //require_once 'classesa4\\' .$class . '.php';
    // ..\classes\DB3\DB3.php
});

// Let's set up some variables for our form:
// This determines whether or not the user has been authenticated for this site
$isAuthenticated = false;
// Determines if the form has been posted yet
$isPosted = $_SERVER['REQUEST_METHOD'] === 'POST';


// Generate a Member object and fill it with the form input if the user
// has posted the form
$member = new Member();
if ($isPosted)
{
    $member->email = $_POST['email'];
    $member->password = $_POST['password'];
    //$member->type = $_POST['type'];
}

// Check if the form the user posted is valid. If it is then
// check the database to see if the specified member exists
// NOTE: Only check the username and password, as there is no
// 'type' field.
$isValidPost = $isPosted && $member->validate_email() && $member->validate_password();
if ( $isValidPost )
{
    // Create the database so that we can find the memeber
    $db = new DB3A4('dba4/assign4.db');
    $db->exec($member->tableDefinition());
    // Check if the member exists in the database
    $logins = $db->selectSome($member, [new Filter('email', $member->email)]);

    // Close and null the database to prevent memory leaks
    $db->close();
    $db = null;

    // Check if there is only one member extracted from the database.
    // Also check if the passwords are the same. This will determine
    // whether our user is authenticated or not.
    $isAuthenticated = count($logins) === 1 && password_verify($member->password, $logins[0]->password);
}

// If our user is not authenticated, build the form that will be used on the page
if ( !$isAuthenticated )
{
    // Make the email field
    $emailInput = new Input5('email', 'email', !$isValidPost ? $member->email : '');
    $emailInput->addLabel($member->getLabel('email'));
    $emailInput->addError($isPosted && !$member->validate_email(), $member->getError('email'));

    // Make the password field
    $passwordInput = new Input5('password', 'password', !$isValidPost ? $member->password : '');
    $passwordInput->addLabel($member->getLabel('password'));
    $passwordInput->addError($isPosted && !$member->validate_password(), $member->getError('password'));
}
// if the user is authenticated, let's setup a new session and redirect
else
{
    // Let's regenerate a new session id
    session_regenerate_id();

    // Let's save the current member into a session variable
    $_SESSION['member'] = $logins[0];

    // Redirect to the filelist page
    header('Location: filelist.php');
    // stop execution of the rest of the code on this page
    die('Member is not authenticated');
}

#endregion
?>
<!DOCTYPE html>
<html>
<head>
    <title>Could Share Login</title>
    <link type="text/css" rel="stylesheet" href="assign4.css" />
</head>
<body>
    <!-- Navbar -->
    <?php require_once('generic/navbar.php') ?>
    <div class="main">
        <h1>Welcome to Cloud Share!</h1>
        <h2>Please enter your credentials</h2>

        <form action="#" method="post">
            <div>
                <?php $emailInput->render(); ?>
            </div>
            <div>
                <?php $passwordInput->render(); ?>
            </div>
            <div>
                <input type="submit" value="Login!" />
            </div>
        </form>

        <?php if ( $isPosted && !$isAuthenticated && $isValidPost ) { ?>
        <div class="error">
            Credentials couldn't be found for <?= htmlentities($member->email) ?>.
        </div>
        <?php } ?>
    </div>
</body>
</html>